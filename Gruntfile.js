module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {},
      dest: {
        files: {
          'assets/js/thib.js': 'src/js/thib.js'
        }
      }
    },

    cssmin: {
      options: {
        keepSpecialComments: 0
      },
      combine: {
        files: {
          'assets/css/thib.css': [
            'src/css/normalize.css',
            'src/css/base.css',
            'src/css/grid.css',
            
            'src/css/main.css'
          ],
          // 'static/assets/css/3rd.css': ['assets/src/css/3rd.css']
          'assets/css/thibnews.css': 'src/css/thibnews.css',
          'assets/css/thibprod.css': 'src/css/thibprod.css',
          'assets/css/thibcontact.css': 'src/css/thibcontact.css'
        }
      }
    },

    watch: {
      scripts: {
        files: ['src/js/*.js'],
        tasks: 'uglify'
      },
      css: {
        files: 'src/css/*.css',
        tasks: 'cssmin'
      }
    }

    // copy: {
    //   main: {
    //     files: [
    //       {
    //         expand: true,
    //         cwd: './assets/dest/',
    //         src: ['**'],
    //         dest: 'e:/works/253/wlink/assets/'
    //       }
    //     ]
    //   }
    // }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['uglify', 'cssmin']);
  grunt.registerTask('js', ['uglify']);
  grunt.registerTask('css', ['cssmin']);
  
  grunt.event.on('watch', function(action, filepath, target) {
    grunt.log.writeln('>=>' + target + ': ' + filepath + ' has ' + action);
  });
};