
$(function () {
  'use strict';
  var $img = new Image(),
  $hdBg = $('.header-bg'),
  $hdImg = $('#hdimg'),
  src = $hdImg.attr('src'),
  $sections = $('.section'),
  secLen = $sections.length,
  
  /**
   * section 路由，带css3动画转场效果
   * @param  {string} pageId section页面的 ID
   */
  router = function(pageId, title) {
    var $target = $('#'+ pageId);

      if ($target.is('.section-active')) {return;}

    title = title || $('.menu-a[data-target='+pageId+']').eq(0).text() || null;

    // 检查一下有没有元素 及 有没有设置 data-target
    if ( $target.size() ) {
      $sections.filter('.section-active').removeClass('section-active').addClass('section-out');
      $target.addClass('section-active section-forward');

      History.pushState( null, title, '?p='+ pageId );
    }
  },

  /**
   * 序列化 localtion.search
   * @return {Object} 返回序列化后的对象
   */
  queryToObj = function () {
    if (!location.search) {return {};}

    var query = location.search.slice(1).split('&'),
    queryObj = {},
    q;

    for (var i = query.length - 1; i >= 0; i--) {
      q = query[i].split('=');
      queryObj[q[0]] = q[1];
    }
    return queryObj;
  };

  // perload callback 相当于页面初始化
  function callback () {
    window.scrollTo(0,0);
    $('body').removeClass('no-ready').addClass('ready');
    $('.load-cover').addClass('hide');
    $hdBg.addClass('hdBgFadeIn');
    $('.header-container').addClass('hdFadeInUp');
    $($img).remove();
  }

  // preload
  $img.onload = function() {
    callback();
  };
  $img.src = src;

  // 页面载入时的转场
  $(window).bind('load', function() {
    var qObj = queryToObj(),
        page = qObj.p || 'home';

    if ( typeof page !== 'undefined' ) {
      router(page);
    }
  });


  // 导航
  $('body').delegate('.menu-a, .fbn-text', 'click', function(event) {
    event.preventDefault();
    var target = $(this).data('target');

    router(target);
  });

  // 动画事件
  $('body').delegate('.section', 'animationend animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(event) {
    $(this).removeClass('section-forward section-back section-in section-out');
  });


  // 下一屏
  function nextSection() {
    var $sections = $('.section'),
    $active = $sections.filter('.section-active'),
    idx = $active.index(),
    nextIdx = (idx + secLen + 1) % secLen,
    $next = $sections.eq( nextIdx );

    $active.removeClass('section-active').addClass('section-out');
    $next.addClass('section-active section-forward');
  }
  // 前一屏
  function prevSection () {
    var $sections = $('.section'),
    $active = $sections.filter('.section-active'),
    idx = $active.index(),
    nextIdx = (idx + secLen - 1) % secLen,
    $next = $sections.eq( nextIdx );

    $active.removeClass('section-active').addClass('section-in');
    $next.addClass('section-active section-back');
  }
  // key nav support
  $(window).bind('keyup', function(event) {
    var k = event.which;
    switch(k) {
      case 37:
        prevSection();
        break;
      case 39:
        nextSection();
        break;
    }
  });

  // footer nav
  $('#footer-list').delegate('.fbn-text', 'mouseenter', function() {
    $(this).parent().addClass('dl-hover');
  })
  .delegate('.fbn-item', 'mouseleave', function() {
    $(this).removeClass('dl-hover');
  });

  // 产品页 卡片
  $('#prod-wrapper').delegate('.collection-list a', 'mouseenter', function() {
    $(this).find('.flip').toggleClass('front');
    $(this).find('.flip').toggleClass('back');
  })
  .delegate('.collection-list a', 'mouseleave', function() {
    $(this).find('.flip').toggleClass('front');
    $(this).find('.flip').toggleClass('back');
  });
  $('#J_collTogller').click(function() {
    $(this).prev().slideToggle(360);
  });
});


// plugins settings
$LAB
.script('assets/3rd/anyslider/jquery.anyslider.min.js')
.wait(function () {
  $('#news-slider').AnySlider({
    interval: 3000,
    speed: 'slow',
    bullets: false
  });
});
$LAB.script('assets/3rd/jQueryTimelinr/js/jquery.timelinr-0.9.54.js')
.wait(function () {
  $().timelinr({
    issuesSpeed: 300,
    datesSpeed:  100,
    arrowKeys:   true
  });
});