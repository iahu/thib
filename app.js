// global & env
var express = require('express');
var port = process.env.VCAP_APP_PORT || 3000;
var fs = require('fs');


// express server
var app = express();
// 开发模式，true 可以根据 规则列表 把 src 的 js、css 重定向到 assets 下
var dev = true;

if (dev) {
    var cssRedirectRule = 'normalize base grid main footer thibnews thib thibcontact thibprod timeline'.split(' ');
    var jsRedirectRule = ['thib'];
    cssRedirectRule.forEach(function (item) {
        app.get('/assets/css/'+ item +'.css', function (req, res) {
            res.writeHead(200, {
                "content-type": "text/css"
            });
            fs.createReadStream('./src/css/'+ item +'.css').pipe(res);
            // console.log('redirect '+ item +'.');
        });
    });
    jsRedirectRule.forEach(function (item) {
        app.get('/assets/js/'+ item +'.js', function (req, res) {
            res.writeHead(200, {
                "content-type": "text/javascript"
            });
            fs.createReadStream('./src/js/'+ item +'.js').pipe(res);
            // console.log('redirect '+ item +'.');
        });
    });
}


// 指定模板引擎并注册 .html 为模板后缀
app.engine('html', require('ejs').renderFile);
// 设置视图模板的默认后缀名为 .html
app.set('view engine', 'html');
// 设置模版路径
app.set('views', __dirname + '/src/views');
// 静态资源
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/dev', express.static(__dirname + '/src'));

app.listen(port);
console.log('server listening on port:', port);

////////////
// render //
////////////
var data = require('./data.js');
app.get('/index.html', function (req, res) {
   res.render('index', {
    BASE_PATH: data.BASE_PATH,
    title: 'Thibault',
    menu: data.menu,
    collection: data.collection
   });    
});
app.get('/', function (req, res) {
   res.render('index', {
    BASE_PATH: data.BASE_PATH,
    title: 'Thibault',
    menu: data.menu,
    collection: data.collection
   });
});

app.get('/news', function (req, res) {
    res.render('news', {
        title: 'Thibault 新聞',
        menu: data.menu
    });
});

app.get('/contact', function (req, res) {
    res.render('contact', {
        title: 'Thibault 联系我们',
        menu: data.menu
    });
});

app.get('/prod', function (req, res) {
    res.render('prod', {
        title: 'Thibault 产品頁',
        menu: data.menu
    });
});
